Welcome

# Web Fundamentals

## 1 Development Environments & Web Programming Languages && IDE

#### objectives:
* Learn how a web server works
* Learn how client languages (HTML/CSS/JS) and server languages (Python, C#, NodeJS) can make interactive websites

---
Project 1: get started with source control.

## 2 MVC Design Pattern 
#### objectives:
* Review HTML 5 and CSS 3
* Introduce MVC 

---
Project 2: run a simple web server localhost

# Design Principles and User Experience

## 3 Information Architecture and Grid Layouts  
#### objectives:
* Introduce building websites with grids
* UI guidelines
* User Experience  
* Cover common tools to plan and organize content
 
---
Project 3: Information Architecture exercise.


## 4 - Layouts with CSS

#### objectives
* JSON 
* Build common layouts and user interfaces with CSS and HTML
* Identify more advanced layouts


## 5 Javascript Basics
     objectives:

* Basic syntax of js
* Project 4: Add Basic javascript to JS to local webserver

## 6 More UI with Javascript

#### objectives

* Document Object Model(DOM)
* Select and navigation of the DOM
* Use the core DOM events 
* Effects, Animations and Scrolling
* Client Side Data Mgmt and Storage

## 7  Front End Development Tools
#### objectives
* CSS Preprocessors
	* Less
	* SASS
* Package manager
* Task Runners: Grunt and Gulp
* Debug JS

## 7 Databases

#### objectives
 
* Overview of database types
* What database to use
* Connecting your project to a database
* ORM

## 8  Forms and Input
#### objectives
* Create-Read-Update-Delete with Form Submit

## 9 Forms and Input pt 2
Project 5: CRUD Application

## 10 - Ajax, Callbacks and Promises
#### objectives
* Create-Read-Update-Delete with Ajax

## 10 - CRUD 
Project 5: Other CRUD Application / Start Final Projecct

## 11 - Search Engine Optimization 

## 12 - Responsive Websites
#### objectives
* learn how websites can adapt to diffrent device screen sizes 

## 13 - User Accounts and Security
#### objectives
* basics authentication and security
* cookies

## 14 - Hosting the Website
#### objectives
* understand the next steps to host a live site 
## 15 - Other

## Project Final
